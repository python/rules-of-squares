import matplotlib.pyplot as plt
import matplotlib.patches as pch
import numpy
import random as rn

def carre(ax=None):
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(10,10))
    ax.plot([0, 0, 17, 17 ,0], [0, 17, 17, 0 ,0], 'k-')
    plt.axis('off')
    return ax




print("Entrez la liste des c/s, un par ligne : ")
rules = []    
for i in range(0,8):
            ele = input().lower()
            rules.append(ele) # adding the element

OR=(8,8)

ax = carre()
ax.add_patch(pch.Rectangle(OR, 1, 1, color="black"));

PosGen = [OR]
Genie = [OR]
POS = ()

colors = ["yellow", "pink", "orange", "brown", "red", "orchid", "blueviolet", "navy"]

for elem in rules :
    n = rn.randint(0,len(colors)-1)
    col = colors[n]
    del colors[n]
    PosGen2 = []
    if elem == 'c' :
        for pos in PosGen :
            touche = 0
            POS = (pos[0]+1,pos[1])
            place = 0
            for posPlace in Genie :
                if POS[0] == posPlace[0] and POS[1] == posPlace[1] : place += 1
            for posTouch in PosGen :
                if POS[0]+1 == posTouch[0] and POS[1] == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1] == posTouch[1] : touche += 1
                if POS[1]+1 == posTouch[1] and POS[0] == posTouch[0] : touche += 1
                if POS[1]-1 == posTouch[1] and POS[0] == posTouch[0] : touche += 1
            if touche <= 1 and place == 0 :
                Genie.append(POS)
                PosGen2.append(POS)
                ax.add_patch(pch.Rectangle(POS, 1, 1, color=col));
            # 
            touche = 0
            POS = (pos[0]-1,pos[1])
            place = 0
            for posPlace in Genie :
                if POS[0] == posPlace[0] and POS[1] == posPlace[1] : place += 1
            for posTouch in PosGen :
                if POS[0]+1 == posTouch[0] and POS[1] == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1] == posTouch[1] : touche += 1
                if POS[1]+1 == posTouch[1] and POS[0] == posTouch[0] : touche += 1
                if POS[1]-1 == posTouch[1] and POS[0] == posTouch[0] : touche += 1
                 
            if touche <= 1 and place == 0 :
                Genie.append(POS)
                PosGen2.append(POS)
                ax.add_patch(pch.Rectangle(POS, 1, 1, color=col));
            # 
            touche = 0
            POS = (pos[0],pos[1]+1)
            place = 0
            for posPlace in Genie :
                if POS[0] == posPlace[0] and POS[1] == posPlace[1] : place += 1
            for posTouch in PosGen :
                if POS[0]+1 == posTouch[0] and POS[1] == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1] == posTouch[1] : touche += 1
                if POS[1]+1 == posTouch[1] and POS[0] == posTouch[0] : touche += 1
                if POS[1]-1 == posTouch[1] and POS[0] == posTouch[0] : touche += 1
            
            if touche <= 1 and place == 0 :
                Genie.append(POS)
                PosGen2.append(POS)
                ax.add_patch(pch.Rectangle(POS, 1, 1, color=col));
            # 
            touche = 0
            POS = (pos[0],pos[1]-1)
            place = 0
            for posPlace in Genie :
                if POS[0] == posPlace[0] and POS[1] == posPlace[1] : place += 1
            for posTouch in PosGen :
                if POS[0]+1 == posTouch[0] and POS[1] == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1] == posTouch[1] : touche += 1
                if POS[1]+1 == posTouch[1] and POS[0] == posTouch[0] : touche += 1
                if POS[1]-1 == posTouch[1] and POS[0] == posTouch[0] : touche += 1
                 
            if touche <= 1 and place == 0 :
                Genie.append(POS)
                PosGen2.append(POS)
                ax.add_patch(pch.Rectangle(POS, 1, 1, color=col));
                
                
    elif elem == 's' :
        for pos in PosGen :
            touche = 0
            POS = (pos[0]+1,pos[1]+1)
            place = 0
            for posPlace in Genie :
                if POS[0] == posPlace[0] and POS[1] == posPlace[1] : place += 1
            for posTouch in PosGen :
                if POS[0]+1 == posTouch[0] and POS[1]+1 == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1]+1 == posTouch[1] : touche += 1
                if POS[0]+1 == posTouch[0] and POS[1]-1 == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1]-1 == posTouch[1] : touche += 1
                 
            if touche <= 1 and place == 0 :
                Genie.append(POS)
                PosGen2.append(POS)
                ax.add_patch(pch.Rectangle(POS, 1, 1, color=col));
            # 
            touche = 0
            POS = (pos[0]+1,pos[1]-1)
            place = 0
            for posPlace in Genie :
                if POS[0] == posPlace[0] and POS[1] == posPlace[1] : place += 1
            for posTouch in PosGen :
                if POS[0]+1 == posTouch[0] and POS[1]+1 == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1]+1 == posTouch[1] : touche += 1
                if POS[0]+1 == posTouch[0] and POS[1]-1 == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1]-1 == posTouch[1] : touche += 1
                 
            if touche <= 1 and place == 0 :
                Genie.append(POS)
                PosGen2.append(POS)
                ax.add_patch(pch.Rectangle(POS, 1, 1, color=col));
            # 
            touche = 0
            POS = (pos[0]-1,pos[1]+1)
            place = 0
            for posPlace in Genie :
                if POS[0] == posPlace[0] and POS[1] == posPlace[1] : place += 1
            for posTouch in PosGen :
                if POS[0]+1 == posTouch[0] and POS[1]+1 == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1]+1 == posTouch[1] : touche += 1
                if POS[0]+1 == posTouch[0] and POS[1]-1 == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1]-1 == posTouch[1] : touche += 1
                 
            if touche <= 1 and place == 0 :
                Genie.append(POS)
                PosGen2.append(POS)
                ax.add_patch(pch.Rectangle(POS, 1, 1, color=col));
            # 
            touche = 0
            POS = (pos[0]-1,pos[1]-1)
            place = 0
            for posPlace in Genie :
                if POS[0] == posPlace[0] and POS[1] == posPlace[1] : place += 1
            for posTouch in PosGen :
                if POS[0]+1 == posTouch[0] and POS[1]+1 == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1]+1 == posTouch[1] : touche += 1
                if POS[0]+1 == posTouch[0] and POS[1]-1 == posTouch[1] : touche += 1
                if POS[0]-1 == posTouch[0] and POS[1]-1 == posTouch[1] : touche += 1
                 
            if touche <= 1 and place == 0 :
                Genie.append(POS)
                PosGen2.append(POS)
                ax.add_patch(pch.Rectangle(POS, 1, 1, color=col));

    else : print('ERREUR : règle inconnue. Recommencez.')
    
    PosGen = PosGen2
